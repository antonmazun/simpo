// import Vue from 'vue';
import $ from 'jquery';
// import AdminApp from './js/AdminApp.vue';
import AuthUser from './js/auth_user';
import Home from './js/home';
import FilterPage from './js/FilterPage';
import FilterAd from './js/FilterAd';
import Cabinet from './js/Cabinet';
import ContactPage from './js/ContactPage';
import Statistic from './js/statistics';
import ChangeInfo from './js/changeInfo';

// Vue.config.productionTip = false;


class Root {
    constructor() {
        this.auth_user = new AuthUser();
        this.home = new Home();
        this.filter_ad = new FilterAd();
        this.filter_page = new FilterPage();
        this.cabinet = new Cabinet();
        this.contact_page = new ContactPage();
        this.statistic = new Statistic();
        this.change_info = new ChangeInfo();
    }

    init() {

    }
}

$(document).ready(function () {

});
window.$ = $;
window.ClientApp = new Root();


//
// new Vue({
//   render: h => h(AdminApp)
// }).$mount('#admin_app');