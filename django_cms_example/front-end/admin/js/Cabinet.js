export default class Cabinet {
    constructor() {
        this.status_btn = $('.js-check-status');
    }


    filter_by_status() {
        this.status_btn.on('change', function () {
            let value_radio  = this.value;
            if (value_radio === 'all'){
                $('.js-template-cabinet').show();
                return;
            } else{
                let selector  = `.js-template-cabinet[data-publish="${value_radio}"]`;
                $('.js-template-cabinet').hide();
                $(selector).show();
                return;
            }
        })
    }
}