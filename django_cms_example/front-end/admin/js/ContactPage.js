export default class ContactPage {
    constructor() {
        this.popup = $('.js-popup-query');
        this.items = $('.js-show-info');
        this.status_radio = $('.js-check-status');
    }

    init() {
        console.log('ContactPage');
        this.popupRules(this.popup, this.items);
        this.sendStatus();
    }

    popupRules(popup, table_row) {
        table_row.on('click', function () {
            let current_query = $(this);
            $(popup).removeClass('hidden');
            $(popup).find('.js-theme').text(current_query.data('theme'));
            $(popup).find('.js-template').text(current_query.data('template'));
            $(popup).find('.js-email').text(current_query.data('email'));
            $(popup).find('.js-phone').text(current_query.data('phone'));
            $(popup).find('.js-name').text(current_query.data('name_human'));
            $(popup).find('.js-message').text(current_query.data('message'));
            console.log(current_query.data('id'));
            $('#js-hidden-inp').val(current_query.data('id'));
            if (current_query.data('status') === 'True') {
                $('#js-check').prop('checked', 'checked');
                $('#js-check').prop('disabled', 'disabled');
            } else {
                $('#js-check').prop('checked', false);
                $('#js-check').prop('disabled', false);
            }
        });
        $('.js-close-query').click(function () {
            $(popup).addClass('hidden');
        });
        console.log(popup);
    }

    sendStatus() {
        $('#js-send-check').click(function (e) {
            let id_query = $('#js-hidden-inp').val();
            if (!$('#js-check').is(':checked')) {
                return;
            } else {
                let url = `/set_status/${id_query}/`;
                $.ajax({
                    url: url,
                    method: 'GET'
                }).done(function (response) {
                    if (response.status) {
                        $('.js-popup-query').addClass('hidden');
                        let current_row_selector = `.js-show-info[data-id="${id_query}"]`;
                        $(current_row_selector).data('status' , 'True');
                        $(current_row_selector).find('.js-change-type').prop('checked', true);
                        console.log();
                    }
                    console.log(response);
                })

            }
        })
    }

    filterRow() {
        this.status_radio.on('change', function () {
            console.log(this);
            let value_radio  = this.value;
            if (value_radio === 'all'){
                $('.js-show-info').show();
                return;
            } else{
                let selector  = `.js-show-info[data-status="${value_radio}"]`;
                $('.js-show-info').hide();
                $(selector).show();
                return;
            }
        })
    }


}