export default class FilterAd {
    constructor() {
        this.left_btns = $('.js-filter-type');
    }

    init() {
        console.log('FilterAd init');
        this.filter_block(this.left_btns);
    }

    filter_block(btns) {
        let component = this;
        console.log(component);
        btns.on('click', function () {
            component.left_btns.removeClass('active');
            $(this).addClass('active');
            let data_filter = $(this).data('filter-category');
            if (data_filter == -1) {
                $('.js-user-template').show();
                return;
            }
            else {
                let selector_showing_block = `.js-user-template[data-target-category="${data_filter}"]`;
                $('.js-user-template').hide();
                $(selector_showing_block).show();
            }

        })
    }
}