var global_filter_category = -1;
var global_checkboxes_type = [];
export default class FilterPage {
    constructor() {
        this.left_btn = $('.js-filter-type');
    }


    init() {
        console.log('FilterPage init!');
        this.filtering_block(this.left_btn);
        this.checkbox_change();
        this.loop_checkbox();

    }

    loop_checkbox() {
        global_checkboxes_type = [];
        $.each($('.js-change-type:checked'), function (index, elem) {
            global_checkboxes_type.push(elem.value);
        });
    }

    trigger_select() {
        let component = this;
        $('.js-select').on('change', function () {
            $(component.left_btn).trigger('click', this.value);
        })
    }

    filtering_block(btns) {
        let component = this;
        btns.on('click', function (e, value) {
            if (value) {
                global_filter_category = value;
                component.global_filter(global_checkboxes_type, global_filter_category);
                return;
            }
            component.left_btn.removeClass('active');
            $(this).addClass('active');
            let filter_data = $(this).data('filter-category');
            global_filter_category = filter_data;
            component.global_filter(global_checkboxes_type, global_filter_category);
        })
    }

    global_filter(checkboxes, category) {
        let array_block = [];
        $.each(global_checkboxes_type, function (index, type) {
            let selector_block = '';
            if (category == -1) {
                selector_block = `.js-user-template[data-target-type="${type}"]`;
            } else {
                selector_block = `.js-user-template[data-target-type="${type}"][data-target-category="${global_filter_category}"]`;
            }
            if ($(selector_block)) {
                array_block.push.apply(array_block, $(selector_block));
            }
        });
        $('.js-user-template').hide();
        if (array_block) {
            $.each(array_block, function (index, elem) {
                $(elem).show();
            })
        } else {

        }

    }

    checkbox_change() {
        let component = this;
        $('.js-change-type').on('change', function (e) {
            component.loop_checkbox();
            if (global_filter_category == -1) {
                component.global_filter(global_checkboxes_type, global_filter_category);
            } else {
                component.global_filter(global_checkboxes_type, global_filter_category);
            }
        })
    }
}