export  default  class ChangeInfo{
    constructor(){
        this.form  = $('#change_form');
        this.password_error  = $('#js_error');
    }

    preSubmit(form){
        let component  = this;
        form.on('submit' , function(e){
           let password_1 =  form.find('#password_1').val();
           let password_2 =  form.find('#password_2').val();
           if (password_1 !== password_2){
               e.preventDefault();
               component.password_error.removeClass('hidden');
               form.find('#password_1').val('');
               form.find('#password_2').val('');
           }
        })
    }

    init(){
        console.log('ChangeInfo');
        this.preSubmit(this.form);
    }
}