import $ from 'jquery';
import Flickity from 'flickity';

export default class Home {
    constructor() {
        this.btn_mobile = $('.js-mobile-menu');
    }

    init() {
        const jQueryBridget = require('jquery-bridget');
        const Flickity = require('flickity');
        jQueryBridget('flickity', Flickity, $);
        this.init_slider();
        this.toggle_mobile_menu(this.btn_mobile);
        // this.searchToggle();

    }

    toggle_mobile_menu(btn) {
        let height_menu_fix = $('.menu_ul li').first().innerHeight() * 5;
        let counter_click = 0;
        let height_menu = 0;
        btn.on('click', function (e) {
            counter_click++;
            if (counter_click % 2 === 1) {
                height_menu = height_menu_fix;
            } else {
                height_menu = 0;
            }
            $('.menu_ul').css({
                'height': height_menu + 'px'
            });
            $('.menu_ul').toggleClass('visible');
            console.log();
        });
        // console.log(btn);
    }

    init_slider() {
        $('.slide--parent').flickity({
            imagesLoaded: true,
            wrapAround: true,
            autoPlay: false,
            pauseAutoPlayOnHover: false
        });
    }

    addListenerForm(form) {
        form.on('submit', function (event) {
            event.preventDefault();
            $.ajax({
                method: $(this).prop('method'),
                url: $(this).prop('action'),
                data: $(this).serialize()
            }).done(function (response) {
                console.log(response);
                if (response.ok) {
                    $('.js-popup-assign').addClass('hidden');
                    window.location.href = '/cabinet/';
                }
            })
        });
    }

    assign_template() {
        let component = this;
        $('.js-assign-template').click(function (e) {
            e.preventDefault();
            let send_url = $(this).prop('href');
            $.ajax({
                method: 'GET',
                url: send_url
            }).done(function (response) {
                let popup = $('.js-popup-assign');
                popup.removeClass('hidden');
                popup.find('.form-user').html(response);
                // component.addListenerForm(popup.find('form'));
            });
        });

        $('.js-close-assign').click(function (e) {
            $('.js-popup-assign').addClass('hidden');
        })
    }


    ajax_view_count() {
        $('.js-count-view').on('click', function (e) {
            $.ajax({
                method: 'GET',
                url: '/statistic/' + $(this).data('page-id') + '/'
            }).done(function(response){
                console.log(response);
            })
        })
    }


}