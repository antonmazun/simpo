import Inputmask from "inputmask";
import {getCookie, setCookie} from "../../cookies.js";

export default class CallBackForm {
    constructor() {
        this.call_back_form = $('.js-call-back-form');
    }

    init() {
        this.submit_form(this.call_back_form);
    }

    mask_phone() {
        let selector = document.getElementById("js_mask_phone");
        let im = new Inputmask("+380(99) 999-9999");
        im.mask(selector);
    }

    mask_email() {
        let selector_email = $('#js_mask_email');
        let im = new Inputmask("*{1,20}[.*{1,20}][.*{1,20}][.*{1,20}]@*{1,20}[.*{2,6}][.*{1,2}]");
        im.mask(selector_email);
    }

    submit_form(form) {
        console.log(form);
        form.on('submit', function (e) {
            e.preventDefault();
            let csrf_token = getCookie("csrftoken");
            console.log(e);
            let form = this;
            console.log(this.action);
            console.log($(this).serialize());
            $(form).addClass('blured');
            $.ajax({
                url: this.action,
                method: this.method,
                data: $(this).serialize(),
                headers: {
                    "X-CSRFToken": csrf_token
                }
            }).done(function (response) {
                if(response.send){
                     $(form).removeClass('blured');
                     $(form).hide();
                }
                console.log(response.send);
            })
        })
    }

}