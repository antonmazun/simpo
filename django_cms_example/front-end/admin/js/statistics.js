export default class Statistic {
    constructor() {
        this.stat_item = $('.js-stat-item');
    }

    init() {
        // console.log('Statistic init');
        this.assoc_item_value(this.stat_item);
        this.diagram_init();
    }

    assoc_item_value(stat_item) {
        let component = this;
        $.each(stat_item, function (index, elem) {
            let html_id = $(elem).data('html-id');
            let month = $(elem).data('month-param').replace('[' , '').replace(']' , '').replace(/'/g , '').replace(/ /g , '').split(',');
            let count = $(elem).data('count-param');
            let name = $(elem).data('name');
            component.diagram_init({
                'selector': html_id,
                'month': month,
                'count': count,
                'name': name
            })})
    }

    diagram_init(settings) {
        Highcharts.setOptions({
            colors: ['#D94F5C', '#BC2F3C']
        });
        Highcharts.chart(settings['selector'], {
            chart: {
                type: 'spline'
            },

            title: {
                text: 'Chart of views'
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                title: {
                    text: 'Months'
                },
                categories: settings['month'],
                labels: {
                    overflow: 'justify'
                }
            },
            yAxis: {
                title: {
                    text: 'Count views'
                },
                labels: {
                    formatter: function () {
                        return this.value;
                    }
                }
            },
            tooltip: {
                crosshairs: true,
                shared: true
            },
            plotOptions: {
                spline: {
                    marker: {
                        radius: 4,
                        lineColor: '#BC2F3C ',
                        lineWidth: 2
                    }
                },
            },
            series: [{
                name: settings['name'],
                label: {
                    onArea: true
                },
                dataLabels: {
                    enabled: true,

                },

                marker: {
                    symbol: 'square'
                },
                data: settings['count']

            }]
        });

    }
}