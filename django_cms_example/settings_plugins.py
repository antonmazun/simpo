gettext = lambda s: s
CMS_PLACEHOLDER_CONF = {
    'col_sidebar': {
        'plugins': ('FilePlugin', 'FlashPlugin', 'LinkPlugin', 'PicturePlugin', 'TextPlugin', 'SnippetPlugin'),
        'name': gettext("sidebar column")
    },

    'col_left': {
        'plugins': (
            'FilePlugin', 'FlashPlugin', 'LinkPlugin', 'PicturePlugin', 'TextPlugin', 'SnippetPlugin',
            'GoogleMapPlugin',
            'CMSTextWithTitlePlugin', 'CMSGalleryPlugin'),
        'name': gettext("left column")
    },

    'col_right': {
        'plugins': (
            'FilePlugin', 'FlashPlugin', 'LinkPlugin', 'PicturePlugin', 'TextPlugin', 'SnippetPlugin',
            'GoogleMapPlugin',),
        'name': gettext("right column")
    },
    'footer': {
        'plugins': ('HelloPlugin',)
    },
    'form': {
        'plugins': ('CallBackFormPlugin',)
    },
    'review': {
        'plugins': ('ReviewPlugins',)
    },
    'cover_img_txt': {
        'plugins': ('HeaderCoverImgTextPlugin',)
    },
    'title_text': {
        'plugins': ('TitleTextPlugin100', 'GoogleMapPlugin', 'PicturePlugin', 'FlashPlugin', 'MultiColumnPlugin')
    },
    'col': {
        'plugins': ('HeaderCoverImgTextColPlugin', 'MultiColumnPlugin')
    },
    'text_title_50': {
        'plugins': ('TitleTextPlugin50',)
    },
    'col_50': {
        'plugins': ('BackCoverImg', 'TitleTextPlufgin100', 'MultiColumnPlugin', 'PicturePlugin')
    },
    'video_image': {
        'plugins': ('VideoPlugin', 'HeaderCoverImgTextPlugin')
    },
    'bus_card_avatar': {
        'plugins': ('BusCardAvatarPlugin',)
    },
    'bus_card_top_right_img': {
        'plugins': ('BusCardAvatarOneImagePlugin' , )
    },
    'bus_card_two_image': {
        'plugins': ('BusCardAvatarTwoImagePlugin',  )
    },
    'test': {
        'plugins': ('ArticlePluginModelPlugin' , )
    },
    'slider_image': {
        'plugins': ('SlidePluginModelPlugin' , )
    },
    'text_title_image': {
        'plugins': ('TitleImageTextPlugin' , )
    },
    'text' : {
        'plugins': ('TextPlugin' , )
    },
    'popup_item': {
        'plugins': ('PopupPluginModelPlugin' , )
    }
}


CMS_TEMPLATES = (
    ## Customize this
    ('fullwidth.html', 'Fullwidth'),
    ('sidebar_left.html', 'Sidebar Left'),
    ('sidebar_right.html', 'Sidebar Right'),
    ('contacts.html', 'Contacts'),
    ('new_html.html', 'Новый'),
    ('slider.html', 'Slider'),
    ('templates/appkit1.html', 'Appkit1'),
    ('templates/spa_halcyn.html', 'Одностраничник'),
    ('templates/presentation_video_image.html', 'Презентация видео или картинка'),
    ('templates/bus_card_avatar.html', 'Визитка с аватаром'),
    ('templates/bus_card_one_img.html', 'Визитка с фоткой справа вверху и аватаром'),
    ('templates/bus_card_two_image.html', 'Визитка с фоткой справа вверху и снизу и аватаром'),
    ('templates/test.html', 'test'),
    ('templates/uvfilm.html', 'cars'),
)