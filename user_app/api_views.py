from django.views import View
from django.shortcuts import render_to_response, get_object_or_404
from django.utils import timezone
from django.db.models import Sum
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
# Для работы с моделями статей у меня используется модуль knowledge
from .models import PageByUser, PageByUserStatistic, CallBack
from django.shortcuts import render, redirect


# тогда как для отображения статей используется модуль post
# Это было сделано для того, чтобы URL статей был следующего вида
#    /post/42/ - где 42 - это id статьи в базе данных
class EArticleView(View):
    # template_name = 'knowledge/article.html'  # Шаблон статьи

    def get(self, request, *args, **kwargs):
        print("GEEEEEEEEEEEEEET!!!!!")
        page_stat = get_object_or_404(PageByUser, id=self.kwargs['page_id'])  # Забираем статью из базы данных
        context = {}

        # Далее забираем объект сегодняшней статистики или создаём новый, если требуется
        obj, created = PageByUserStatistic.objects.get_or_create(
            defaults={
                "page_stat": page_stat,
                "date": timezone.now()
            },
            # При этом определяем, забор объекта статистики или его создание
            # по двум полям: дата и внешний ключ на статью
            date=timezone.now(), page_stat=page_stat
        )

        obj.views += 1  # инкрементируем счётчик просмотров и обновляем поле в базе данных
        obj.save(update_fields=['views'])

        # А теперь забираем список 5 последний самых популярных статей за неделю
        popular = PageByUserStatistic.objects.filter(
            # отфильтровываем записи за последние 7 дней
            date__range=[timezone.now() - timezone.timedelta(7), timezone.now()]
        ).values(
            # Забираем интересующие нас поля, а именно id и заголовок
            # К сожалению забрать объект по внешнему ключу в данном случае не получится
            # Только конкретные поля из объекта
            'page_stat_id'
        ).annotate(
            # Суммируем записи по просмотрам
            # Всё суммируется корректно с соответствием по запрашиваемым полям объектов
            views=Sum('views')
        ).order_by(
            # отсортируем записи по убыванию
            '-views')[:5]  # Заберём последние пять записей

        context['popular_list'] = popular  # Отправим в контекст список статей
        if request.is_ajax():
            print('ajax!')
            return JsonResponse({
                'status': 'ok'
            })
        else:
            return render(request, 'user_app/partials/statistic.html', context)


def set_status(request, query_id):
    print("query_id ", query_id)
    current_query = CallBack.objects.get(pk=int(query_id))
    current_query.status = True
    current_query.save()
    return JsonResponse({
        'status': True
    })
