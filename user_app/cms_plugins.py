from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from .models import Hello, CallBackForm, \
    ReviewItem , ArticlePluginModel ,\
    HeaderCoverImgText , TitleText , Video ,\
    BusCardAvatar , BusCardAvatarOneImage, BusCardAvatarTwoImage ,\
    PollPluginModel , SlidePluginModel , PopupPluginModel


from cms.models import Page, PageType, Title


@plugin_pool.register_plugin
class HelloPlugin(CMSPluginBase):
    model = Hello
    name = _("Hello Plugin")
    render_template = "hello_plugin.html"
    cache = False

    def render(self, context, instance, placeholder):
        context = super(HelloPlugin, self).render(context, instance, placeholder)
        return context


import json


@plugin_pool.register_plugin
class CallBackFormPlugin(CMSPluginBase):
    model = CallBackForm
    name = _('Call back form')
    render_template = 'call_back_form.html'
    cache = False

    def render(self, context, instance, placeholder):
        context = super(CallBackFormPlugin, self).render(context, instance, placeholder)
        create_by = None
        for elem in context:
            try:
                create_by = User.objects.get(username=elem.get('current_page').created_by).id
                context.update({
                    'create_by': create_by
                })
                break
                # print(elem.get('current_page').created_by)
            except Exception as e:
                print('error ', e)
        return context


@plugin_pool.register_plugin
class ReviewPlugins(CMSPluginBase):
    model = ReviewItem
    name = _('Review plugin')
    render_template = 'plugins/reviews.html'
    cache = False

    def render(self, context, instance, placeholder):
        context = super(ReviewPlugins, self).render(context, instance, placeholder)
        return context

@plugin_pool.register_plugin
class ArticlePluginModelPlugin(CMSPluginBase):
    model = ArticlePluginModel
    name = _('ArticlePluginModelPlugin')
    render_template = 'plugins/assoc.html'
    cache = False

    def render(self, context, instance, placeholder):
        context = super(ArticlePluginModelPlugin, self).render(context, instance, placeholder)
        return context

@plugin_pool.register_plugin
class SlidePluginModelPlugin(CMSPluginBase):
    model  = SlidePluginModel
    name = _('SlidePluginModelPlugin')
    render_template = 'plugins/image_slide.html'
    cache = False

    def render(self, context, instance, placeholder):
        context = super(SlidePluginModelPlugin, self).render(context, instance, placeholder)
        return context

@plugin_pool.register_plugin
class PopupPluginModelPlugin(CMSPluginBase):
    model  = PopupPluginModel
    name = _('PopupPluginModelPlugin')
    render_template = 'plugins/title_text_image__plugins/popup_text_image.html'
    cache = False

    def render(self, context, instance, placeholder):
        context = super(PopupPluginModelPlugin, self).render(context, instance, placeholder)
        return context

@plugin_pool.register_plugin
class HeaderCoverImgTextPlugin(CMSPluginBase):
    model = HeaderCoverImgText
    name = _('HeaderCoverImgText')
    render_template = 'plugins/header_cover_img_txt.html'
    cache = False

    def render(self, context, instance, placeholder):
        context = super(HeaderCoverImgTextPlugin, self).render(context, instance, placeholder)
        return context

@plugin_pool.register_plugin
class TitleImageTextPlugin(CMSPluginBase):
    model = HeaderCoverImgText
    name = _('TitleImageTextPlugin')
    render_template = 'plugins/title_text_image__plugins/titlte_text_image_1.html'
    cache = False

    def render(self, context, instance, placeholder):
        context = super(TitleImageTextPlugin, self).render(context, instance, placeholder)
        return context


class PopupPlugin(CMSPluginBase):
    model = HeaderCoverImgText
    name = _('PopupPlugin')
    render_template = 'plugins/title_text_image__plugins/popup_text_image.html'
    cache = False

    def render(self, context, instance, placeholder):
        context = super(PopupPlugin, self).render(context, instance, placeholder)
        return context


@plugin_pool.register_plugin
class TitleTextPlugin100(CMSPluginBase):
    model = TitleText
    name = _('TitleText')
    render_template = 'plugins/text_title__plugin/title_text_100.html'
    cache = False

    def render(self, context, instance, placeholder):
        context = super(TitleTextPlugin100, self).render(context, instance, placeholder)
        return context

@plugin_pool.register_plugin
class TitleTextPlugin50(CMSPluginBase):
    model = TitleText
    name = _('TitleText_50')
    render_template = 'plugins/text_title__plugin/title_text_50.html'
    cache = False

    def render(self, context, instance, placeholder):
        context = super(TitleTextPlugin50, self).render(context, instance, placeholder)
        return context


@plugin_pool.register_plugin
class HeaderCoverImgTextColPlugin(CMSPluginBase):
    model = HeaderCoverImgText
    name = _('Text_Title_Image')
    render_template = 'plugins/title_text_image__plugins/image_circle_text.html'
    cache = False

    def render(self, context, instance, placeholder):
        context = super(HeaderCoverImgTextColPlugin, self).render(context, instance, placeholder)
        return context

@plugin_pool.register_plugin
class BackCoverImg(CMSPluginBase):
    model = HeaderCoverImgText
    name = _('BackCoverImg')
    render_template = 'plugins/back_image__plugins/back_cover_img.html'
    cache = False

    def render(self, context, instance, placeholder):
        context = super(BackCoverImg, self).render(context, instance, placeholder)
        return context

@plugin_pool.register_plugin
class VideoPlugin(CMSPluginBase):
    model = Video
    name = _('Video')
    render_template = 'plugins/video.html'
    cache = False

    def render(self, context, instance, placeholder):
        context = super(VideoPlugin, self).render(context, instance, placeholder)
        return context

@plugin_pool.register_plugin
class BusCardAvatarPlugin(CMSPluginBase):
    model = BusCardAvatar
    name = _('BusCardAvatar')
    render_template = 'plugins/bussines_card/bus_card_avatar.html'
    cache = False

    def render(self, context, instance, placeholder):
        context = super(BusCardAvatarPlugin, self).render(context, instance, placeholder)
        return context



@plugin_pool.register_plugin
class BusCardAvatarOneImagePlugin(CMSPluginBase):
    model = BusCardAvatarOneImage
    name = _('BusCardAvatarOneImage')
    render_template = 'plugins/bussines_card/bus_card_top_right_img.html'
    cache = False

    def render(self, context, instance, placeholder):
        context = super(BusCardAvatarOneImagePlugin, self).render(context, instance, placeholder)
        return context


@plugin_pool.register_plugin
class BusCardAvatarTwoImagePlugin(CMSPluginBase):
    model = BusCardAvatarTwoImage
    name = _('BusCardAvatarTwoImage')
    render_template = 'plugins/bussines_card/b_c_two_img.html'
    cache = False

    def render(self, context, instance, placeholder):
        context = super(BusCardAvatarTwoImagePlugin, self).render(context, instance, placeholder)
        return context


@plugin_pool.register_plugin  # register the plugin
class PollPluginPublisher(CMSPluginBase):
    model = PollPluginModel  # model where plugin data are saved
    module = _("Polls")
    name = _("Poll Plugin")  # name of the plugin in the interface
    render_template = "plugins/assoc.html"

    def render(self, context, instance, placeholder):
        context.update({'instance': instance})
        return context