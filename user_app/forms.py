from cms.models.pagemodel import Page, PageType
import django.forms as forms


class PageForm(forms.ModelForm):
    class Meta:
        model = Page
        # fields = '__all__'
        exclude = ['slug']

    def save(self, commit=True):
        print('save!!!!!!')


from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User

my_default_errors = {
    'required': 'This field is required',
    'invalid': 'Enter a valid value'
}

# < a
# href = "{% url 'user-login' %}"

#
# class ="personal_info js-auth-show header-icon user-icon" > Log in < img src="{% static 'in.svg' %}"
#
#
# alt = "" > < / a >

class SignUpForm(UserCreationForm):
    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')

    def __init__(self, *args, **kwargs):
        super(SignUpForm, self).__init__(*args, **kwargs)
        self.fields['email'].label = "Ваш e-mail"
        self.fields['username'].label = "Логін"
        self.fields['password1'].label = "Пароль"
        self.fields['password2'].label = "Пітвердіть пароль"
        self.fields['password2'].widget.attrs['class'] = 'password'
        self.fields['password1'].widget.attrs['class'] = 'password'

        # self.fields['username'](error_messages=my_default_errors)

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2',)


class CustomAuthenticationForm(AuthenticationForm):
    def __init__(self, *args, **kwargs):
        super(CustomAuthenticationForm, self).__init__(*args, **kwargs)
        self.fields['username'] = 'Логін'
        self.fields['password'] = 'password1'
