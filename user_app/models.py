from cms.models.pluginmodel import CMSPlugin
from cms.models.pagemodel import Page

from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

from djangocms_text_ckeditor.fields import HTMLField

class Author(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, default='')
    avatar = models.ImageField(upload_to='avatar', blank=True,
                               null=True, default='')
    description = models.CharField(max_length=5000, verbose_name='About me', blank=True,
                                   null=True, default='')
    full_name = models.CharField(max_length=300, verbose_name='Full name', blank=True,
                                 null=True, default='')
    about_me = models.TextField(max_length=600, verbose_name='About me', blank=True,
                                null=True, default='')


class Hello(CMSPlugin):
    guest_name = models.CharField(max_length=50, default='Guest')


class CallBackForm(CMSPlugin):
    email = models.CharField(max_length=255, verbose_name='E-mail', blank=True, null=True, default='')
    phone = models.CharField(max_length=255, verbose_name='Phone', blank=True, null=True, default='')


class ReviewItem(CMSPlugin):
    image = models.ImageField(upload_to='review_image',
                              verbose_name='Photo review',
                              blank=True, null=True)
    text = models.TextField(max_length=1555, verbose_name='Message review', blank=True, null=True)
    author_review = models.CharField(max_length=255, verbose_name='author review', blank=True, null=True)
    country_author = models.CharField(max_length=255, verbose_name='country', blank=True, null=True)
    txt = models.CharField(max_length=10)


class PageByUser(models.Model):
    page_user_custom = models.ForeignKey(Page, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    category = models.TextField(max_length=1000, verbose_name='Category')
    image = models.ImageField(upload_to='image_template', blank=True, null=True)

    def __str__(self):
        return self.category + ' ' + self.user.username + ' ' + self.page_user_custom.get_title()

    def views(self, **kwargs):
        return Statistic.objects.filter(page=self)

    def count_sum(self):
        count_sum = 0
        for page in self.views():
            count_sum += page.count
        return count_sum

    def check_published(self):
        current_page = Page.objects.get(pagebyuser=self)
        return current_page.is_published('ru')


class PageByUserStatistic(models.Model):
    page_stat = models.ForeignKey(PageByUser)  # внешний ключ на статью
    date = models.DateField('Дата', default=timezone.now)  # дата
    views = models.IntegerField('Просмотры', default=0)  # количество просмотров в эту дату

    def __str__(self):
        return str(self.date)


class Statistic(models.Model):
    MONTH = (
        ('Jan', 'January'),
        ('Feb', 'February'),
        ('Mar', 'March'),
        ('Apr', 'April'),
        ('May', 'May'),
        ('Jun', 'June'),
        ('Jul', 'July'),
        ('Aug', 'August'),
        ('Sep', 'September'),
        ('Oct', 'October'),
        ('Nov', 'November'),
        ('Dec', 'December')
    )
    page = models.ForeignKey(PageByUser, on_delete=models.CASCADE)
    month = models.CharField(max_length=55, choices=MONTH)
    count = models.IntegerField()

    def __str__(self):
        return 'month - {} page-{}; count -{}'.format(self.month, self.page, self.count)


class CallBack(models.Model):
    page = models.ForeignKey(Page, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    theme = models.CharField(max_length=255, verbose_name='Theme message')
    email = models.CharField(max_length=255, verbose_name='Email')
    phone = models.CharField(max_length=255, verbose_name='Phone')
    name_human = models.CharField(max_length=255, verbose_name='Name user')
    message = models.TextField(max_length=10000, verbose_name='Message')
    status = models.BooleanField(verbose_name='Viewed', default=False)
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return 'page is {} theme is {}'.format(self.page, self.theme)


class ArticlePluginModel(CMSPlugin):
    title = models.CharField(max_length=50)

    def copy_relations(self, oldinstance):
        print('copy_relations')
        for associated_item in oldinstance.associated_item.all():
            # instance.pk = None; instance.pk.save() is the slightly odd but
            # standard Django way of copying a saved model instance
            associated_item.pk = None
            associated_item.plugin = self
            associated_item.save()

    def render(self, context, instance, placeholder):
        print('render!!!!!!!!!!!')
        # context.update({
        #     'product': instance.product,
        #     'object': instance,
        #     'placeholder': placeholder
        # })

        return context


class SlidePluginModel(CMSPlugin):
    image = models.ImageField(upload_to='slides_images')

    def copy_relations(self, oldinstance):
        print('copy_relations')
        for associated_item in oldinstance.SliderImage_item.all():
            # instance.pk = None; instance.pk.save() is the slightly odd but
            # standard Django way of copying a saved model instance
            associated_item.pk = None
            associated_item.plugin = self
            associated_item.save()

    def render(self, context, instance, placeholder):
        # print('render!!!!!!!!!!!')
        return context


class SliderImage(models.Model):
    plugin = models.ForeignKey(
        SlidePluginModel,
        related_name="SliderImage_item"
    )


class PopupPluginModel(CMSPlugin):
    image = models.ImageField(upload_to='popup_slides')
    title  = models.CharField(max_length=255 , verbose_name='Short description' , blank=True , null=True , default='')
    text = HTMLField(configuration='CKEDITOR_SETTINGS_MODEL1', default='')


    def copy_relations(self, oldinstance):
        print('copy_relations')
        for associated_item in oldinstance.popup_item.all():
            # instance.pk = None; instance.pk.save() is the slightly odd but
            # standard Django way of copying a saved model instance
            associated_item.pk = None
            associated_item.plugin = self
            associated_item.save()

class Popup(models.Model):
    plugin  = models.ForeignKey(PopupPluginModel , related_name="popup_item" )

class AssociatedItem(models.Model):
    plugin = models.ForeignKey(
        ArticlePluginModel,
        related_name="associated_item"
    )


class HeaderCoverImgText(CMSPlugin):
    image = models.ImageField(upload_to='header_user_img',
                              verbose_name='header_user_img',
                              blank=True, null=True)
    title = models.CharField(max_length=255, verbose_name='Title', blank=True, null=True)
    text = models.TextField(max_length=500, verbose_name='Short description', blank=True, null=True)


class TitleText(CMSPlugin):
    title = models.CharField(max_length=1000, verbose_name='Title block')
    text = models.TextField(max_length=5000, verbose_name='description', blank=True, null=True)


class Video(CMSPlugin):
    file = models.FileField(upload_to='pres_video', blank=True, null=True)
    autoplay = models.BooleanField(default=True, verbose_name='autoplay')
    controls = models.BooleanField(default=False, verbose_name='Show controls')


#



class BusCardAvatar(CMSPlugin):
    avatar = models.ImageField(upload_to='user_avatar', default='', blank=True, null=True, )
    background = models.ImageField(upload_to='back_buss_card', default='', blank=True, null=True, )
    title = models.CharField(max_length=200, verbose_name='Title', default='')
    text = HTMLField(configuration='CKEDITOR_SETTINGS_MODEL1', default='')
    phone_1 = models.CharField(max_length=20, verbose_name='First phone number',
                               blank=True, null=True, default='')
    phone_2 = models.CharField(max_length=20, verbose_name='Second phone number',
                               blank=True, null=True, default='')
    phone_3 = models.CharField(max_length=20, verbose_name='Third phone number',
                               blank=True, null=True, default='')
    email = models.CharField(max_length=50, verbose_name='Email',
                             blank=True, null=True, default='')
    address = models.CharField(max_length=255, verbose_name='Address',
                               blank=True, null=True, default='')


class BusCardAvatarOneImage(CMSPlugin):
    avatar = models.ImageField(upload_to='user_avatar', default='')
    background = models.ImageField(upload_to='back_buss_card', default='')
    right_top_image = models.ImageField(upload_to='back_buss_card', default='')
    title = models.CharField(max_length=200, verbose_name='Title', default='')
    description = HTMLField(configuration='CKEDITOR_SETTINGS_MODEL1', default='')
    text = HTMLField(configuration='CKEDITOR_SETTINGS_MODEL1', default='')
    phone_1 = models.CharField(max_length=20, verbose_name='First phone number',
                               blank=True, null=True, default='')
    phone_2 = models.CharField(max_length=20, verbose_name='Second phone number',
                               blank=True, null=True, default='')
    phone_3 = models.CharField(max_length=20, verbose_name='Third phone number',
                               blank=True, null=True, default='')
    email = models.CharField(max_length=50, verbose_name='Email',
                             blank=True, null=True, default='')
    address = models.CharField(max_length=255, verbose_name='Address',
                               blank=True, null=True, default='')


class BusCardAvatarTwoImage(CMSPlugin):
    avatar = models.ImageField(upload_to='user_avatar', default='')
    background = models.ImageField(upload_to='back_buss_card', default='')
    right_top_image = models.ImageField(upload_to='back_buss_card', default='')
    right_bottom_image = models.ImageField(upload_to='back_buss_card', default='')
    title = models.CharField(max_length=200, verbose_name='Title', default='')
    description = HTMLField(configuration='CKEDITOR_SETTINGS_MODEL1', default='')
    text = HTMLField(configuration='CKEDITOR_SETTINGS_MODEL1', default='')
    phone_1 = models.CharField(max_length=20, verbose_name='First phone number',
                               blank=True, null=True, default='')
    phone_2 = models.CharField(max_length=20, verbose_name='Second phone number',
                               blank=True, null=True, default='')
    phone_3 = models.CharField(max_length=20, verbose_name='Third phone number',
                               blank=True, null=True, default='')
    email = models.CharField(max_length=50, verbose_name='Email',
                             blank=True, null=True, default='')
    address = models.CharField(max_length=255, verbose_name='Address',
                               blank=True, null=True, default='')


from solo.models import SingletonModel


class SiteConfiguration(SingletonModel):
    header_title = models.CharField(max_length=255, default='Title in header')
    header_text = models.TextField(max_length=1555, default='', verbose_name='Text in header')
    btn_text = models.CharField(max_length=255, default='', verbose_name='Text in button')
    footer_title = models.CharField(max_length=255, default='', verbose_name='Title in footer')
    footer_text = models.TextField(max_length=1500, default='', verbose_name='Text in footer')

    def __str__(self):
        return "Site Configuration"

    class Meta:
        verbose_name = "Site Configuration"


class Slider(models.Model):
    image = models.ImageField(upload_to='slider')
    title = models.CharField(max_length=255, verbose_name='Title slide')
    short_description = models.CharField(max_length=255, verbose_name='Short description for slide')
    text = HTMLField(configuration='CKEDITOR_SETTINGS_MODEL1', default='')


class Poll(models.Model):
    name = models.CharField(max_length=255, verbose_name='name')
    age = models.IntegerField()


class PollPluginModel(CMSPlugin):
    poll = models.ForeignKey(Poll)

    def __str__(self):
        return self.poll.question
