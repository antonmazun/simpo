from django import template
from cms.models.pagemodel import Page
from django.contrib.auth.models import User
from ..models import Statistic

register = template.Library()


def cms_navigation():
    cms_pages = Page.objects.filter(in_navigation=True)
    return {'cms_pages': cms_pages}


register.inclusion_tag('user_app/index.html')(cms_navigation)


@register.filter('check_page_perm')
def check_page_perm(request):
    current_page = None
    try:
        current_user = User.objects.get(id=request.user.id)
        current_page = Page.objects.filter(created_by=current_user)
    except Exception as e:
        print(e)
    try:
        full_path_request = request.get_full_path()[:request.get_full_path().index('?')]
    except ValueError as e:
        full_path_request = request.get_full_path()

    if current_page:
        for page in current_page:
            if page.get_absolute_url() == full_path_request:
                return 'visible'
    return 'django-hidden-toolbar'


@register.filter('review_class')
def review_class(id):
    if int(id) % 2 == 0:
        return
    else:
        return 'item-reversed'


@register.filter('month_stat')
def month_stat(id_page, options):
    print('----------------')
    stat_page = Statistic.objects.filter(page__id=int(id_page)).values('month', 'count')
    param_month = []
    param_count = []
    for elem in stat_page:
        param_month.append(elem['month'])
        param_count.append(int(elem['count']))
    if options == 'month':
        return (param_month)
    elif options == 'count':
        return param_count
