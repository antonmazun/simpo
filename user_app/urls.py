# -*- coding: utf-8 -*-
from __future__ import absolute_import, print_function, unicode_literals

from cms.sitemaps import CMSSitemap
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.contrib.sitemaps.views import sitemap
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.views.static import serve
from . import views, auth_views, api_views

admin.autodiscover()

urlpatterns = [
    url(r'^$', views.index),
    # url(r'^signup/$', views.signup, name='user-register'),
    # url(r'^logout/$', views._logout, name='logout'),
    # url(r'^login/$', views._login, name='login'),
    url(r'cabinet/$', views.cabinet, name='cabinet'),
    url(r'show-template/(?P<id>[0-9]+)/$', views.show_template, name='show-template'),
    url(r'set-template/(?P<id_template>[0-9]+)/$', views.set_template, name='set-template'),
    url(r'contact-form/$', views.contact_form, name='contact-form'),
    url(r'start/$', views.start, name='start'),
    url(r'about-us/$', views.about, name='about'),
    url(r'advertisement/$', views.advertisement, name='advertisement'),
    url(r'templates/$', views.templates, name='templates'),
    url(r'user-profile/$', views.user_profile, name='user_profile'),
    url(r'contactus/$', views.contactus, name='contactus'),
    url(r'my-profile/$', views.my_profile, name='my-profile'),
    url(r'contact-form-page/$', views.contact_form_page, name='contact_form_page'),
    url(r'create-new/$', views.create_new, name='create-new'),
    url(r'^statistic/$', views.statistics, name='statistics'),
    url(r'^profile-form/$', views.profile_form, name='profile-form'),

]

auth_urls = [
    url(r'user-login/$', auth_views.user_login, name='user-login'),
    url(r'user-register/$', auth_views.user_register, name='user-register'),
    url(r'user-logout/$', auth_views.user_logout, name='user-logout'),
]

api_urls = [
    url(r'^statistic/(?P<page_id>[0-9]+)/$', api_views.EArticleView.as_view()),

    url(r'^set_status/(?P<query_id>[0-9]+)/$', api_views.set_status),
]

urlpatterns += auth_urls
urlpatterns += api_urls
