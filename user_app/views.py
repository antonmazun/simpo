from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django import template
from cms.models.pagemodel import Page
from cms.models.permissionmodels import PageUser
from cms.api import create_page
from cms.forms.wizards import CreateCMSPageForm
from cms.admin.forms import AddPageForm
from cms.models import Page, PageType, Title
from cms.admin.forms import AddPageForm
from .forms import SignUpForm
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.forms import UserCreationForm

from django.contrib.auth.models import User

from .models import PageByUserStatistic, Statistic, Slider, Author

base_context = {
    'register_form': SignUpForm,

}

# register = template.Library()
# Create your views here. Lib/site-packages/cms/models/permissionmodels.py


#
# class PageType(Page):
#
#     class Meta:
#         proxy = True
#         default_permissions = []
#
#     @classmethod
#     def get_root_page(cls, site):
#         pages = Page.objects.on_site(site).filter(
#             node__depth=1,
#             is_page_type=True,
#         )
#         return pages.first()
#
#     def is_potential_home(self):
#         return False


from cms.admin import pageadmin
from cms.admin.forms import AddPageTypeForm
from cms.api import publish_page, get_page_draft


# Lib/site-packages/cms/cms_wizards.py

def index(request, **kwargs):
    print(**kwargs)
    # print(Page.objects.all())
    # print('len' , len(Page.objects.public()))

    # important !!!!!!!!!!!!!!!!!!!!!!!!!!!
    # print(publish_page)
    #     # for page in Page.objects.all():
    #     #     publish_page(page=get_page_draft(page) , user=request.user , language='ru')
    #     #     print(page)

    # important !!!!!!!!!!!!!!!!!!!!!!!!!!!
    # print(PageUser.objects.all())
    # print(cms_navigation())
    # print(Page.objects.all().filter(created_by=request.user))
    # print("PageType.objects.all() " , PageType.objects.all())
    # for pagetype  in PageType.objects.all():
    #     print(pagetype.get_title_obj())
    categories_pages = {}

    # print(Page.objects.drafts())

    # level_1 = PageType.objects.filter(is_page_type=True , node__depth=1)[1:]

    # print('level222222')
    # for item in level_2:
    #     # print('----------')
    #     # print(item.get_parent_page().get_title())
    #     print(item.get_child_pages())
    #     # print(item.id)
    #     print('----------')
    # print('level3--------')
    # for elem in level_3:
    #     print(elem.get_title())
    #     print(elem.get_parent_page().id)
    # for page in Page.objects.public():
    #     print(page.created_by)
    # print('-------------')
    # print(categories_pages)
    # print('-------------')
    # for elem in categories_pages:
    #
    #     print(elem)

    # print('form is ' ,  )
    from .forms import PageForm

    # from cms.api import publish_pages

    # print(len(list(publish_pages())))
    ctx = {}
    ctx['all_slides'] = Slider.objects.all()
    ctx.update(base_context)
    return render(request, 'user_app/pages/home.html', ctx)


def show_template(request, id):
    current_page = Page.objects.get(id=id)
    template_for_page = current_page.get_template()
    title = current_page.get_title()
    print(current_page.get_slug())
    print(type(template_for_page))
    ctx = {
        'id_template': id,
        'title': title,
        'page': current_page,
        'choice_template_btn_flag': True,
        'disabled': True
    }
    ctx.update(base_context)
    return render(request, template_for_page, ctx)


from cms.api import create_page
from .models import PageByUser


def set_template(request, id_template=''):
    if request.method == 'GET':
        print(CreateCMSPageForm)
        current_page = Page.objects.get(id=id_template)
        print(current_page.get_title())
        type_pages = Page.objects.filter(
            is_page_type=True,
            publisher_is_draft=True,
        )
        level_2 = PageType.objects.filter(is_page_type=True, node__depth=2)
        # root_page = PageType.get_root_page(site=self._site)

        # if True:
        #     print('root!')
        #     # Set the choicefield's choices to the various page_types
        #     descendants = root_page.get_descendant_pages().filter(is_page_type=True)
        #     titles = Title.objects.filter(page__in=descendants, language=self._language)
        #     print(titles)
        #     choices = [('', '---------')]
        #     choices.extend((title.page_id, title.title) for title in titles)
        #     source_field.choices = choices
        title_arr = []
        for elem in level_2:
            if elem.get_child_pages():
                for child_page in elem.get_child_pages():
                    title_arr.append(child_page.get_title())

        title_arr = list(set(title_arr))

        ctx = {'form': AddPageForm,
               'type_pages': type_pages,
               'level_2': title_arr,
               'current_page': current_page,
               'id_template': id_template}
        ctx.update(base_context)
        return render(request, 'user_app/add_form.html', ctx)
    elif request.method == 'POST':
        from django.template.defaultfilters import slugify
        from unidecode import unidecode
        title = request.POST.get('title')
        description = request.POST.get('description')
        print()
        # page_title = request.POST.get('page_title')
        type_page_id = request.POST.get('type_page')
        print("slugify ", slugify(unidecode(title)))
        print(Page.objects.get(pk=int(type_page_id)).get_template())
        page = create_page(title=title,
                           meta_description=description,
                           language='ru',
                           template=Page.objects.get(pk=int(type_page_id)).get_template(),
                           created_by=request.user,
                           slug=slugify(unidecode(title)))

        page_by_user = PageByUser.objects.create(page_user_custom=page,
                                                 user=request.user,
                                                 category=request.POST.get('parent_page'),
                                                 image=request.FILES['image'])

        PageByUserStatistic.objects.create(page_stat=page_by_user)
        for index in range(12):
            Statistic.objects.create(page=page_by_user, month=Statistic.MONTH[index][0], count=0)
        # print(PageByUser.objects.all())
        # print(Page.objects.get(pk=int(request.POST.get('parent_page'))))
        username = request.user.username
        password = request.user.password
        # logout(request)
        # user = authenticate(username=username, password=password)
        # print(user)
        # login(request , user)
        # print(request.user.username , request.user.password)
        return HttpResponseRedirect('/cabinet/')


def _login(request):
    if request.method == 'GET':
        ctx = {}
        ctx.update(base_context)
        return render(request, 'user_app/login.html', ctx)
    elif request.method == 'POST':
        print('asdasdasdsa')
        username = request.POST.get('login')
        password = request.POST.get('password')
        print(username, password)
        user = authenticate(username=username, password=password)
        if user:
            login(request, user)
            return redirect('cabinet')


from django.template.loader import get_template
from django.template import Context
from django.template.loader import render_to_string
from django.core.mail import EmailMultiAlternatives
from .models import CallBack


def contact_form(request):
    author_user_page = User.objects.get(pk=int(request.POST.get('create_by')))
    # print(author_user_page)
    # print(Page.objects.get(id=int(request.POST.get('user_page_id'))))
    # print(request.POST.get('user_page'))
    print('---------------')
    user_mail = request.POST.get('to_email')
    theme = request.POST.get('theme')
    phone = request.POST.get('phone')
    name_user = request.POST.get('name_user')
    user_msg = request.POST.get('msg')
    email = request.POST.get('email')
    context_email = {
        'phone': phone,
        'name_user': name_user,
        'msg': user_msg,
        'email': email,
        'user_page': request.POST.get('user_page'),
    }
    subject, from_email = theme, 'kpi.study1@gmail.com'
    message = render_to_string('snippets/email_template.html', context_email)
    msg = EmailMultiAlternatives(subject, 'test!', from_email, [user_mail])
    msg.attach_alternative(message, "text/html")
    msg.send()
    CallBack.objects.create(
        page=Page.objects.get(id=int(request.POST.get('user_page_id'))),
        user=author_user_page,
        theme=theme,
        email=email,
        phone=phone,
        message=user_msg,
        name_human=name_user
    )
    return JsonResponse({
        'send': True
    })


def start(request):
    ctx = {}
    level_2 = PageType.objects.filter(is_page_type=True, node__depth=2)
    level_3 = PageType.objects.filter(is_page_type=True, node__depth=3)
    ctx['level_2'] = level_2
    ctx['level_3'] = level_3
    ctx.update(base_context)
    return render(request, 'user_app/start.html', ctx)


def signup(request):
    print('signup')
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            u = User(username=username, password=raw_password)
            u.is_active = True
            u.is_superuser = True
            u.save()
            print('is super user', u.is_superuser)
            # User.objects.create(username=username,
            #                     password=raw_password,
            #                     is_superuser=True,
            #                     is_staff=True,
            #                     is_active=True)
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('cabinet')
    else:
        form = UserCreationForm()
    return render(request, 'user_app/signup.html', {'form': form})


def _logout(request):
    logout(request)
    return redirect('/')


def cabinet(request):
    ctx = {}
    if request.user.is_authenticated:
        # ctx['users_pages'] = PageByUser.objects.all().filter(user=request.user)
        # ctx['users_pages'] = PageByUser.objects.filter(pagebyuserstatistic__page_stat__user=
        try:
            user_pages = PageByUser.objects.filter(pagebyuserstatistic__page_stat__user=request.user).distinct()
            ctx['user_pages'] = user_pages
        except Exception as e:
            print(type(e))
        # for elem in ctx['users_pages']:
        #     print(elem.get_absolute_url())
        # print(user_pages)
        ctx['current_page'] = 'my_template'
        ctx.update(base_context)
        return render(request, 'user_app/cabinet.html', ctx)
        # return HttpResponse(ctx)
    # else:
    #     return redirect('signup')


from django.utils import timezone
from django.db.models import Sum


def statistics(request):
    ctx = {}
    if request.user.is_authenticated:

        try:
            user_pages = PageByUser.objects.filter(statistic__page__user=request.user).distinct()
            ctx['user_pages'] = user_pages
        except Exception as e:
            print(type(e))
        print(Statistic.objects.filter(page__user=request.user.id))

        # for stat in Statistic.objects.filter(page__user=request.user.id):
        #     print('id ' , stat.page.id)
        #     print(stat.count)

        # popular = PageByUserStatistic.objects.filter(
        #     # отфильтровываем записи за последние 7 дней
        #     date__range=[timezone.now() - timezone.timedelta(30), timezone.now()],
        #     page_stat__user=request.user.id
        # ).values(
        #     # Забираем интересующие нас поля, а именно id и заголовок
        #     # К сожалению забрать объект по внешнему ключу в данном случае не получится
        #     # Только конкретные поля из объекта
        #     'page_stat_id'
        # ).annotate(
        #     # Суммируем записи по просмотрам
        #     # Всё суммируется корректно с соответствием по запрашиваемым полям объектов
        #     views=Sum('views')
        # ).order_by(
        #     # отсортируем записи по убыванию
        #     '-views')[:5]  # Заберём последние пять записей

        # ctx['popular_list'] = popular
        ctx['current_page'] = 'my_template'
        ctx.update(base_context)
        return render(request, 'user_app/partials/statistic.html', ctx)


def about(request):
    ctx = {}
    ctx.update(base_context)
    return render(request, 'user_app/pages/about.html', ctx)


def advertisement(request):
    ctx = {}
    level_2 = PageType.objects.filter(is_page_type=True, node__depth=2)
    print(PageByUser.objects.filter(page_user_custom__title_set__published=True).distinct())
    # ctx['all_pages'] = Page.objects.drafts().filter(title_set__published=True).distinct()
    ctx['all_pages'] = PageByUser.objects.filter(page_user_custom__title_set__published=True).distinct()
    ctx['level_2'] = level_2
    for page in PageByUser.objects.all():
        print(page.page_user_custom.is_published('ru'))
    print(PageByUser.objects.all())

    title_arr = []
    for elem in level_2:
        if elem.get_child_pages():
            for child_page in elem.get_child_pages():
                title_arr.append(child_page.get_title())

    title_arr = list(set(title_arr))
    ctx['title_arr'] = title_arr
    # for page in ctx['all_pages']:
    #     print(page.get_parent_page())
    ctx.update(base_context)
    return render(request, 'user_app/pages/advertisement.html', ctx)


def templates(request):
    level_2 = PageType.objects.filter(is_page_type=True, node__depth=2)
    level_3 = PageType.objects.filter(is_page_type=True, node__depth=3)
    title_arr = []
    for elem in level_2:
        if elem.get_child_pages():
            for child_page in elem.get_child_pages():
                title_arr.append(child_page.get_title())

    title_arr = list(set(title_arr))
    # for elem in PageType.objects.filter(is_page_type=True, node__depth=2):
    #     print('____________________-')
    #     print(len(elem.get_child_pages()))
    # print(PageType.objects.all())
    ctx = {
        'level_2': level_2,
        'level_3': level_3,
        'title_arr': title_arr
    }
    ctx.update(base_context)
    print(ctx)
    return render(request, 'user_app/pages/templates.html', ctx)


def contactus(request):
    ctx = {}
    ctx.update(base_context)
    return render(request, 'user_app/pages/contactus.html', ctx)


def user_profile(request):
    ctx = {}
    ctx.update(base_context)
    return render(request, 'user_app/pages/user_profile.html', ctx)


def my_profile(request):
    ctx = {}
    ctx['current_page'] = 'profile'
    ctx['curr_user'] = Author.objects.get(user=request.user.id)
    ctx.update(base_context)
    return render(request, 'user_app/pages/profile.html', ctx)


def contact_form_page(request):
    ctx = {}
    ctx['current_page'] = 'contact_form'
    if request.user.is_authenticated:
        all_query = CallBack.objects.filter(user=request.user).order_by('-date')
        ctx['all_query'] = all_query
    ctx.update(base_context)
    return render(request, 'user_app/pages/contact_form.html', ctx)


def create_new(request):
    level_2 = PageType.objects.filter(is_page_type=True, node__depth=2)
    level_3 = PageType.objects.filter(is_page_type=True, node__depth=3)

    # for elem in PageType.objects.filter(is_page_type=True, node__depth=2):
    #     print('____________________-')
    #     print(len(elem.get_child_pages()))
    # print(PageType.objects.all())
    title_arr = []
    for elem in level_2:
        if elem.get_child_pages():
            for child_page in elem.get_child_pages():
                title_arr.append(child_page.get_title())

    title_arr = list(set(title_arr))

    ctx = {
        'level_2': level_2,
        'level_3': level_3,
    }
    ctx['title_arr'] = title_arr
    ctx['current_page'] = 'create_new'
    ctx.update(base_context)
    return render(request, 'user_app/pages/create_new.html', ctx)


from django.contrib.auth.hashers import check_password


def profile_form(request):
    ctx = {}
    author = Author.objects.get(user=request.user.id)
    old_avatar = author.avatar
    ctx['current_page'] = 'profile'
    ctx['curr_user'] = author
    if request.method == 'POST':
        user = User.objects.get(id=request.user.id)
        # print(current_password)
        avatar = request.FILES.get('avatar')
        print(avatar)
        full_name = request.POST.get('full_name')
        old_password = request.POST.get('password')
        new_password = request.POST.get('new_password')
        about_me = request.POST.get('about_me')
        if user.check_password(old_password) and old_password:
            user.set_password(new_password)
            user.save()
            author.about_me = about_me
            author.full_name = full_name
            if avatar:
                author.avatar = avatar
            author.save()
            user = authenticate(username=user.username, password=new_password)
            login(request, user)
            return redirect('my-profile')
        elif avatar:
            author.avatar = avatar
            author.save()
            return redirect('my-profile')
        else:
            ctx['error'] = 'Old password is not correct.'
            return render(request, 'user_app/pages/profile.html', ctx)
